import React from 'react'
import { View, Text } from 'react-native'
import Detailproduct from './screens/detailproduct'
import Home from './screens/home'
import Product from './screens/product'
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


const stack =createNativeStackNavigator();
const Drawer=createDrawerNavigator();

const DrawerRoute = ({navigation})=>{
  return(
   
      <Drawer.Navigator>
        <Drawer.Screen name="Home" component={Home}  options={{headerShown:false}}/>
        <Drawer.Screen name="product" component={Product}  options={{headerShown:false}}/>
        <Drawer.Screen name="detail product" component={Detailproduct}  options={{headerShown:false}}/>

      </Drawer.Navigator>
   
  )
}
const App = () => {
  return (
    // <View>
    //  {/* <Home/> */}
    //  {/* <Product/> */}
    //  <Detailproduct/>
    // </View>
    <>
    <View>
      <Text></Text>
    </View>
    <NavigationContainer>
      <stack.Navigator>
        <stack.Screen name="Home" component={DrawerRoute}  options={{headerShown:false}}></stack.Screen>
      </stack.Navigator>
    </NavigationContainer>
    </>
  )
}

export default App
