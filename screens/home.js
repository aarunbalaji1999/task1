import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TextInput, Card, Title, Button } from 'react-native-paper';
import { Rating } from 'react-native-ratings';

const ratingCompleted = (rating) => {
    return (
        console.log("Rating is: " + rating)
    )

}



const home = ({ navigation }) => {
    const [item, setItems] = useState([])
    const [key, setkey] = useState([])
    const api = () => {



        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'GET',
       
          })
            .then(response => response.json())
            .then(json => {
                setItems(json)
               

            })

    }


    useEffect(() => {
        api()
        console.log(item)

    }, [])


    return (
        <View>
            <View style={{ flexDirection: "row", alignContent: "center", alignItems: "flex-start", justifyContent: "space-between" }}>
                <View style={{ alignSelf: "flex-start", flexDirection: "row" }}>
                    <TouchableOpacity style={{ alignSelf: "flex-start", left: 13, top: 8 }} onpress={() => this.props.navigate.navigation}>

                        <Ionicons

                            name="reorder-four-outline"
                            color="black"
                            size={22}

                            style={{ backgroundColor: 'transparent' }}
                        />

                    </TouchableOpacity>


                    <Text style={{ fontSize: 25, fontWeight: "700", position: "relative", color: "black", left: 30 }}>HOME</Text>
                </View>

                <TouchableOpacity style={{ top: 8, right: 20 }}  >

                    <Ionicons

                        name="notifications"

                        size={22}

                        style={{ backgroundColor: 'transparent' }}
                    />

                </TouchableOpacity>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>

                <View>
                    <Text style={{ fontSize: 34, fontWeight: "700", marginLeft: 15, color: "black" }}>Discover</Text>
                    <Text style={{ fontSize: 14, fontWeight: "400", marginLeft: 17 }}>New collections</Text>
                </View>
                <View>
                    <View style={{ flexDirection: 'row', left: 38, height: 50 }}>
                        <View style={{ width: "65%" }}>
                            <TextInput style={{ margin: 15, backgroundColor: "#F5F7F9", height: 50, fontSize: 12, borderRadius: 8, left: 10, width: 130 }}
                                label="search product..."
                                value=""
                                theme={{ backgroundColor: "#fff" }}

                            />

                        </View>
                        <View style={{ width: "10%", top: 25, right: 45 }}>
                            <Ionicons

                                name="ios-search-outline"

                                size={24}

                                style={{ backgroundColor: 'transparent' }}
                            />
                        </View>
                    </View>
                </View>



            </View>
            <View>
                <Card style={{
                    margin: 10,
                    padding: 40,
                    backgroundColor: "#A278F4",
                    width: 327,
                    height: 159,
                    left: 6

                }}
                >
                    <Title style={{ color: "white" }}>BLACK FRIDAY</Title>
                    <Title style={{ color: "white", fontSize: 15 }}>20% off for all items</Title>
                    <Button style={{ backgroundColor: "#3C444C", margin: 1, padding: 0, width: 100 }} mode="contained">BUYnow</Button>
                </Card>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
                <TouchableOpacity onPress={() => {
                    setkey("Popular")
                 
                }} >

                    <Text style={{ height: 31, alignContent: "flex-start", position: "relative", padding: 0,  }} mode="text" color="black">Popular</Text>

                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    setkey("Popular")
                }} >

                    <Text style={{  height: 31, alignContent: "flex-start", position: "relative", padding: 0 }} >Trendy</Text>

                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    setkey("Newcolection")
                }} >

                    <Text style={{  height: 31, alignContent: "flex-start",  padding: 0 ,}} mode="text" color="black">Newcolection</Text>

                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    setkey("all")
                }} >

                    <Button style={{ backgroundColor: "#3C444C", height: 31, alignContent: "flex-start", position: "relative", padding: 0,bottom:5 }} mode="contained">all</Button>

                </TouchableOpacity>

            </View>
          
            <ScrollView>
            { key==="all"?(
            <View>
                {
                    item.map((item, index) => {
                        return (
                            <Card style={{
                                margin: 10,
                                borderRadius: 5,
                                width: 327,
                                height: 130,
                                left: 6





                            }}>
                                <View>
                                    <View style={{ flexDirection: "row" }}>
                                        <Image style={{ width: 112, height: 131, borderRadius: 5 }} source={require("../assets/4xRoc6.jpg")} />
                                        <View style={{ left: 18, }}>
                                            <Title style={{ fontSize: 16, fontWeight: "500" }}>{item.userId}</Title>
                                            <Title style={{ fontSize: 16, fontWeight: "500", bottom: 15 }}>{item.id}</Title>
                                            <Title style={{ fontWeight: "bold", bottom: 15 }}>{item.id}</Title>
                                            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                                <Rating

                                                    onFinishRating={ratingCompleted()}
                                                    imageSize={16}
                                                    ratingCount={1}
                                                    style={{ paddingVertical: 1, alignItems: "flex-start", size: 30, marginLeft: 2 }}

                                                />
                                                <View>
                                                    <Text style={{ fontSize: 12, top: 2 }}>
                                                        (2638 Reviews)
                                                    </Text>
                                                </View>
                                                <View style={{ left: 50 }}>
                                                    <Rating

                                                        type='heart'
                                                        ratingCount={1}
                                                        imageSize={20}

                                                        onFinishRating={ratingCompleted()}
                                                        style={{ alignItems: "flex-end", marginRight: 10 }}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </View>

                                </View>
                            </Card>

                        )

                    })
                }
                </View>):<View><Text style={{fontSize:30,top:5,color:"red"}}>{"No Data found!!"}</Text></View>}

                
                
            </ScrollView>

        </View>
    )
}

export default home