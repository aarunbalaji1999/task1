import React from 'react'
import { View, Text,TouchableOpacity,Image} from 'react-native'
import { Button } from 'react-native-paper'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TextInput,Card,Title} from 'react-native-paper';
import { Rating } from 'react-native-ratings';


 const ratingCompleted =(rating)=> {
     return(
        console.log("Rating is: " + rating)
     )
    
  }

const detailproduct = () => {
   
    return (
        <View>
            <View style={{flexDirection:"row",alignContent:"center",alignItems:"flex-start",justifyContent:"space-between"}}>
                 <View style={{alignSelf:"flex-start"}}>
                <TouchableOpacity style={{alignSelf:"flex-start",left:10}} >
                           
                           <Ionicons
                         
                               name="arrow-back-outline"
                                color="black"
                                size={24}
                                
                                style={{backgroundColor: 'transparent'}}
                                  />
                                 
                           </TouchableOpacity>
                           </View>
                           
            <Text style={{fontSize:24,fontWeight:"700",color:"black"}}> Detail Product</Text>
           
                       <TouchableOpacity style={{right:15}} >
                           
                           <Ionicons
                         
                               name="ellipsis-vertical-sharp"
                               
                                size={24}
                                color="black"
                                
                                style={{backgroundColor: 'transparent'}}
                                  />
                                 
                           </TouchableOpacity>
             </View>
             <View style={{top:10}}>
              
             <Image style={{width:327,height:265,left:15,borderRadius:5}} source={require("../assets/4xRoc6.jpg")}/>
             

           
             
            
             
         
             </View>

             <View style={{left:15,top:10}}>
                 <Text style={{fontWeight:"700",fontSize:25,top:5,color:"black"}}>
                     Minavi Headset
                     
                 </Text>
                 <Text style={{fontWeight:"700",fontSize:25,color:"black"}}>pro gaming</Text>
                 <View>
                 <Rating
                          
                            onFinishRating={ratingCompleted()}
                            imageSize={18}
                            style={{ paddingVertical: 1,alignItems:"flex-start",size:30,top:5}}
                            />

                            {/* <Rating
                            type='heart'
                            ratingCount={3}
                            imageSize={60}
                            showRating
                            onFinishRating={ratingCompleted()}
                            /> */}
                 </View>
                 <View>
                     <Text style={{fontSize:16,fontWeight:"600",color:"black",top:5}}>
                         Select variant
                     </Text>
                 </View>

             </View>
             <View style={{flexDirection:"row",top:15,left:5}}>
                 <Button style={{margin:10}} mode='contained'>16Hz</Button>
                 <Button  style={{margin:10,}} mode='outlined' color="black">24Hz</Button>
                 <Button  style={{margin:10}} mode='outlined'color="black">32Hz</Button>
             </View>
             <View>
                <Text style={{fontSize:16,fontWeight:"600",top:10,left:14,color:"black"}}>
                    Descriptions
                </Text>
                <View style={{left:1}}>
                <Text style={{left:14,top:10,fontSize:14,fontWeight:"400",}}>
                Lorem ipsum dolor sit amet,Nam nulla purus cursus  </Text>
                <Text style={{left:14,top:10,fontSize:14,fontWeight:"400",}}>
                 Nam nulla purus cursus ;
                     
                </Text>
                </View>
             </View>
             <View style={{flexDirection:"row"}}>
                 <Text style={{left:14,top:10,fontSize:14,fontWeight:"400",color:"black"}}>
                     Read more
                 </Text>
                 <Ionicons
                 name="md-chevron-down"
                 size={20}
                 style={{top:10,left:10,color:"black"}}
                 
                 />
             </View >
             <View style={{flexDirection:"row",justifyContent:"space-between",top:15}}>
                 <Text style={{fontSize:32,fontWeight:"700",color:"black",left:14}}>
                     $30.99
                 </Text>
                 <Button style={{width:110,height:45,right:20,alignContent:"center"}} mode="contained">Buy now</Button>
             </View>
        </View>
    )
}

export default detailproduct
